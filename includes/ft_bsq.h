/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bsq.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:07:28 by eloren-l          #+#    #+#             */
/*   Updated: 2018/10/10 22:50:08 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_BSQ_H
# define FT_BSQ_H

typedef struct	s_param_list
{
	char		empty;
	char		obstc;
	char		full;
	int			colms;
	int			lines;
	int			sqr_sz;
	int			sqr_ln;
	int			sqr_cl;
}				t_param_list;

int				ft_atoi(char *str);
int				ft_find_min(int a, int b, int c);

int				ft_error();
void			ft_putwords(char **words, t_param_list *param);

void			ft_add_sqr(char **map, t_param_list *param);
int				ft_fill_lin(char **map, int **sqr, t_param_list *param);
int				ft_fill_col(char **map, int **sqr, t_param_list *param);

int				ft_first_check(char *argv);
void			ft_free(char **map, int **sqr, t_param_list *param);

int				ft_map(char ***map, int **sqr, t_param_list *param, char *argv);
int				ft_sqr(char **map, int ***sqr, t_param_list *param);

int				ft_write_map(char *argv, char **map, t_param_list *param);
int				ft_find_sqr(char **map, int **sqr, t_param_list *param);

int				ft_input(int argc, char **argv);
void			ft_functions(char **map, int **sqr, t_param_list *param);

#endif

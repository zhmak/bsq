/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strings.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/02 17:11:20 by eloren-l          #+#    #+#             */
/*   Updated: 2018/10/10 22:49:57 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include "ft_bsq.h"

void	ft_putwords(char **words, t_param_list *param)
{
	while (*words != 0)
	{
		write(1, *words, param->colms);
		words++;
		write(1, "\n", 1);
	}
}

int		ft_input(int argc, char **argv)
{
	int		count;
	int		descr;
	char	arr[1024];

	if (argc > 1)
		return (argc);
	descr = open("buffer", O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
	count = 1;
	while (count > 0)
	{
		count = read(0, arr, 1024);
		write(descr, arr, count);
	}
	close(descr);
	argv[1] = (char *)malloc(sizeof(char) * 7);
	argv[1] = "buffer\0";
	return (argc + 1);
}

void	ft_functions(char **map, int **sqr, t_param_list *param)
{
	ft_find_sqr(map, sqr, param);
	ft_add_sqr(map, param);
	ft_putwords(map, param);
	ft_free(map, sqr, param);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_numbers.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/06 20:08:00 by eloren-l          #+#    #+#             */
/*   Updated: 2018/10/10 23:11:32 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include "ft_bsq.h"

int		ft_atoi(char *str)
{
	int result;
	int sign;

	result = 0;
	sign = 0;
	while (*str == '\r' || *str == '\t' || *str == '\n' ||
		*str == '\v' || *str == '\f' || *str == ' ')
		str++;
	while (*str == '-' || *str == '+')
	{
		sign++;
		if (*str == '-')
			sign = sign + 2;
		if (sign == 2 || sign == 4 || sign == 6)
			return (result);
		str++;
	}
	while ((*str >= '0' && *str <= '9'))
	{
		result = result * 10 + *str - '0';
		str++;
	}
	if (sign == 3)
		result = -1 * result;
	return (result);
}

int		ft_first_check(char *argv)
{
	int		descr;
	int		count;
	char	check[1];

	count = 0;
	descr = open(argv, O_RDONLY);
	while (*check != '\n')
	{
		if (!(read(descr, check, 1)))
			break ;
		count++;
	}
	if (count < 5)
		return (ft_error());
	count = 0;
	*check = 'X';
	while (*check != '\n')
	{
		if (!(read(descr, check, 1)))
			break ;
		count++;
	}
	if (count < 2)
		return (ft_error());
	return (1);
}

void	ft_free(char **map, int **sqr, t_param_list *param)
{
	int i;

	i = 0;
	while (i < param->lines)
	{
		free(map[i]);
		free(sqr[i++]);
	}
	free(map);
	free(sqr);
}

int		ft_map(char ***map, int **sqr, t_param_list *param, char *argv)
{
	*map = (char **)malloc(sizeof(char *) * (param->lines + 1));
	if (!(ft_write_map(argv, *map, param)))
	{
		ft_free(*map, sqr, param);
		return (ft_error());
	}
	return (1);
}

int		ft_sqr(char **map, int ***sqr, t_param_list *param)
{
	*sqr = (int **)malloc(sizeof(int *) * (param->lines));
	if (!(ft_fill_lin(map, *sqr, param)))
	{
		ft_free(map, *sqr, param);
		return (ft_error());
	}
	return (1);
}

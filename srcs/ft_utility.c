/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utility.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 15:20:23 by eloren-l          #+#    #+#             */
/*   Updated: 2018/10/10 23:11:30 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_bsq.h"
#include <unistd.h>
#include <stdlib.h>

int		ft_fill_lin(char **map, int **sqr, t_param_list *param)
{
	int i;
	int j;

	i = 0;
	while (i < param->lines)
		sqr[i++] = (int *)malloc(sizeof(int) * param->colms);
	i = 0;
	j = 0;
	while (j < param->colms)
	{
		if (map[i][j] != param->empty && map[i][j] != param->obstc)
			return (ft_error());
		if (map[i][j] == param->empty)
			sqr[i][j] = 1;
		else
			sqr[i][j] = 0;
		j++;
	}
	return (ft_fill_col(map, sqr, param));
}

int		ft_fill_col(char **map, int **sqr, t_param_list *param)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (i < param->lines)
	{
		if (map[i][j] != param->empty && map[i][j] != param->obstc)
			return (ft_error());
		if (map[i][j] == param->empty)
			sqr[i][j] = 1;
		else
			sqr[i][j] = 0;
		i++;
	}
	return (1);
}

int		ft_find_min(int a, int b, int c)
{
	if (a <= b && a <= c)
		return (a);
	if (b <= a && b <= c)
		return (b);
	if (c <= a && c <= b)
		return (c);
	return (0);
}

void	ft_add_sqr(char **map, t_param_list *param)
{
	int buf_cl;
	int buf_ln;

	buf_ln = param->sqr_ln;
	buf_cl = param->sqr_cl;
	while (param->sqr_ln >= (buf_ln - param->sqr_sz + 1))
	{
		param->sqr_cl = buf_cl;
		while (param->sqr_cl >= (buf_cl - param->sqr_sz + 1))
		{
			map[param->sqr_ln][param->sqr_cl] = param->full;
			param->sqr_cl--;
		}
		param->sqr_ln--;
	}
}

int		ft_error(void)
{
	write(2, "map error\n", 10);
	return (0);
}

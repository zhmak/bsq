/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/04 16:23:23 by eloren-l          #+#    #+#             */
/*   Updated: 2018/10/10 23:11:28 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stdio.h"

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "ft_bsq.h"

int		ft_count_col(char *argv, t_param_list *param)
{
	int		descr;
	int		count;
	char	check[1];

	descr = open(argv, O_RDONLY);
	while (*check != '\n')
		if (!(read(descr, check, 1)))
			return (ft_error());
	count = 0;
	*check = 'X';
	while (*check != '\n')
	{
		count++;
		if (!(read(descr, check, 1)))
			return (ft_error());
	}
	close(descr);
	param->colms = count - 1;
	if (param->lines < 1 || param->colms < 1)
		return (ft_error());
	return (1);
}

int		ft_sym_and_lines(char *argv, t_param_list *param)
{
	int		count;
	int		descr;
	char	check[1];
	char	string[1024];

	count = 0;
	descr = open(argv, O_RDONLY);
	while (*check != '\n')
	{
		if (!(read(descr, check, 1)))
			return (ft_error());
		string[count++] = *check;
	}
	count = count - 2;
	param->full = string[count];
	param->obstc = string[--count];
	param->empty = string[--count];
	string[count] = '\0';
	count = -1;
	while (string[++count])
		if (!(string[count] >= 48 && string[count] <= 57))
			return (ft_error());
	param->lines = ft_atoi(string);
	close(descr);
	return (1);
}

int		ft_write_map(char *argv, char **map, t_param_list *param)
{
	int		i;
	int		descr;
	char	check[1];

	map[param->lines] = 0;
	i = 0;
	descr = open(argv, O_RDONLY);
	while (*check != '\n')
		read(descr, check, 1);
	while (i < param->lines)
	{
		map[i] = (char *)malloc(sizeof(char) * (param->colms + 1));
		if (!(read(descr, map[i], (param->colms + 1))))
			return (ft_error());
		if (map[i][param->colms] != '\n')
			return (ft_error());
		map[i][param->colms] = '\0';
		i++;
	}
	close(descr);
	return (1);
}

int		ft_find_sqr(char **map, int **sqr, t_param_list *param)
{
	int i;
	int j;

	i = 0;
	while (++i < param->lines)
	{
		j = 0;
		while (++j < param->colms)
		{
			if (map[i][j] != param->empty && map[i][j] != param->obstc)
				return (ft_error());
			if (map[i][j] == param->empty)
				sqr[i][j] = ft_find_min(sqr[i - 1][j], sqr[i][j - 1],
						sqr[i - 1][j - 1]) + 1;
			else
				sqr[i][j] = 0;
			if (sqr[i][j] > param->sqr_sz)
			{
				param->sqr_sz = sqr[i][j];
				param->sqr_ln = i;
				param->sqr_cl = j;
			}
		}
	}
	return (1);
}

int		main(int argc, char **argv)
{
	int				i;
	int				**sqr;
	char			**map;
	t_param_list	param;

	i = 0;
	argc = ft_input(argc, argv);
	while (++i < argc)
	{
		param.sqr_sz = 0;
		if (!(ft_first_check(argv[i])))
			continue;
		if (!(ft_sym_and_lines(argv[i], &param)))
			continue;
		if (!(ft_count_col(argv[i], &param)))
			continue;
		if (!(ft_map(&map, sqr, &param, argv[i])))
			continue;
		if (!(ft_sqr(map, &sqr, &param)))
			continue;
		ft_functions(map, sqr, &param);
		(i < argc - 1) ? write(1, "\n", 1) : 0;
	}
}

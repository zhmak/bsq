# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: eloren-l <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/09/30 22:24:08 by eloren-l          #+#    #+#              #
#    Updated: 2018/10/10 22:44:19 by eloren-l         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
INCL = includes
SRCS = srcs/*.c
NAME = bsq


all: compile

compile:
	gcc -Wall -Wextra -Werror  main.c $(SRCS) -I$(INCL) -o $(NAME)

clean:
	rm -f $(NAME)

